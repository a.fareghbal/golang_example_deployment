# ![RealWorld Example App](logo.png)


[![Build Status](https://travis-ci.org/wangzitian0/golang-gin-realworld-example-app.svg?branch=master)](https://travis-ci.org/wangzitian0/golang-gin-realworld-example-app)
[![codecov](https://codecov.io/gh/wangzitian0/golang-gin-realworld-example-app/branch/master/graph/badge.svg)](https://codecov.io/gh/wangzitian0/golang-gin-realworld-example-app)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://github.com/gothinkster/golang-gin-realworld-example-app/blob/master/LICENSE)
[![GoDoc](https://godoc.org/github.com/gothinkster/golang-gin-realworld-example-app?status.svg)](https://godoc.org/github.com/gothinkster/golang-gin-realworld-example-app)

> ### Golang/Gin codebase containing real world examples (CRUD, auth, advanced patterns, etc) that adheres to the [RealWorld](https://github.com/gothinkster/realworld) spec and API.


This codebase was created to demonstrate a fully fledged fullstack application built with **Golang/Gin** including CRUD operations, authentication, routing, pagination, and more.


# Directory structure
```
.
├── gorm.db
├── hello.go
├── common
│   ├── utils.go        //small tools function
│   └── database.go     //DB connect manager
├── users
|   ├── models.go       //data models define & DB operation
|   ├── serializers.go  //response computing & format
|   ├── routers.go      //business logic & router binding
|   ├── middlewares.go  //put the before & after logic of handle request
|   └── validators.go   //form/json checker
├── ...
...
```

# Getting started

## Install Golang

Make sure you have Go 1.13 or higher installed.

https://golang.org/doc/install

## Environment Config

Set-up the standard Go environment variables according to latest guidance (see https://golang.org/doc/install#install).


## Install Dependencies
From the project root, run:
```
go build ./...
go test ./...
go mod tidy
```

## Testing
From the project root, run:
```
go test ./...
```
or
```
go test ./... -cover
```
or
```
go test -v ./... -cover
```
depending on whether you want to see test coverage and how verbose the output you want.

## Docker Env and k8s deployment
This project utilizes docker along helm charts to enable an easy CI/CD pipeline based on Jenkins.

### Dockerfile
Based on multilayer image feature I've created a Dockerfile for this project in the fist layer the container tries to build the golang binrary and after that we'll copy the built binrary into a seprate image based on debian image this helm to remove any unneccary requirments in the final docker container.

### HelmCharts
To be able to deploy this application to any k8s cluster there is a helm chart available for this diirectory it includes many of the neccacary features in k8s including:
- basic TCP health-check
- resource limiation
- ingress object
- service object
- hpa

You can use this chart to deploy this app based on your needs to any k8s cluster.

NOTE: You may need to edit the values.yaml or set the variables you need when runing the `helm install` or `helm upgrade --install` command upong you own specifications.

### CI/CD
There is also a sample Jenkinsfile included in the root of the project which uses jenkins pipeline syntax ( groovy ), you can use this template to create a production ready CI/CD.

This pipeline includes the following stages

- git -> clones the repository
- test -> run code coverage tests
- bootstrap k8s env -> create k8s namespace and regisitry secret based on the branch name
- docker build -> builds the docker image and create 2 tags latest-branch-name and ${BUILD_NUMBER}
- docker push -> push docker image to gitlab private registry
- deploy -> deploy k8s deployment with helm charts


To be able to use this exact Jenkinsfile you should create a running jenkins instance with docker, kubectl,helm installed and already have the gitlab registry and k8s client config ready to be used ( configured ).

NOTE: This jenkinsfiles is intented to bootstrap the application even on the first run ( when there is no previous deployment on k8s ) so you don't need to install the app manually for the first run



### Production Consideration

- You can use a pre-built Docker base image that includes all the neccasary dependencies like sqlite3 or tzdata package to have a faster CI/CD pipeline.
- It is better to imeplment health-check in HTTP mode instead of runinng k8s probes in TCP mode
- It is suggested to use DFS or Network Storages when running in production mode for static files so you can make sure that all the data remains intact and you can actually use k8s scaling features.
- As this is a proof of concept for how to dpeloy on k8s we have used sqllite3 which makes any app running in k8s an statefull application, this is something you should avoid at prodiction ( Create your applications in stateless mode :D).
## Todo
- More elegance config
- Test coverage (common & users 100%, article 0%)
- ProtoBuf support
- Code structure optimize (I think some place can use interface)
- Continuous integration (done)

