#!/bin/bash

set -xe
CMD="/usr/local/bin/main"

if [[ "$1" == "" ]]; then
    exec "${CMD}"
else
    exec "$@"
fi
