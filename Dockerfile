FROM golang:buster AS build-env

ENV GOPROXY=direct

WORKDIR /go/src/app

COPY go.* ./

RUN go mod download -x

COPY . .

RUN go build -o main


FROM debian:buster-slim

EXPOSE 8080

RUN apt-get update && apt-get install -y ca-certificates tzdata sqlite3

COPY .docker/docker-entrypoint.sh /docker-entrypoint.sh

# copy the built binrary from build-layer
COPY --from=build-env /go/src/app/main /usr/local/bin/main

ENTRYPOINT [ "/docker-entrypoint.sh" ]