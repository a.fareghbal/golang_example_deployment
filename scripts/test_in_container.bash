#!/bin/bash


run_in_container="docker run --rm --net=host --user=root -v `pwd`:/go/src/app golang:buster"


$run_in_container bash -c """
apt-get update && apt-get install -y ca-certificates tzdata sqlite3
cd /go/src/app
export GOPROXY=direct
go test -v ./... -cover
"""
