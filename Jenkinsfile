String repoUrl = "git@gitlab.com:a.fareghbal/golang_example_deployment.git"
String registryUrl = "registry.gitlab.com"
String team="a.fareghbal"
String app="golang_example_deployment"
String fullRegistryUrl="${registryUrl}/${team}/${app}"
String branch = env.BRANCH_NAME

timestamps {
node () {

    stage('GIT'){
        git branch: "${branch}", credentialsId: 'PUT_CREDENTIALS_ID_HERE', url: "$repoUrl"
    }

    // Run codecoverage script in a container
    stage('Test') {
        sh "./.scripts/tests_in_container.bash"
    }

    // Bootstrap k8s namespaces based on branch name, Also create registry secret
    stage('Bootstrap k8s'){        
        namespace = sh(
            script: "echo ${branch} | cut -d '/' -f 2 | sed 's/_/-/g'",
            returnStdout: true
        ).trim()
        sh """ 
        docker login ${registryUrl} --username=${GITLAB_REGISTRY_USERNAME} --password=${GITLAB_REGISTRY_PASSWORD}
        kubectl create ns ${namespace} || true
        kubectl create secret generic regcred \
            --from-file=.dockerconfigjson=/var/lib/jenkins/.docker/config.json \
            --type=kubernetes.io/dockerconfigjson -n ${namespace} || true
        sed -i "s/namespace.golang-test.example.com/${namespace}.golang-test.example.com/g" ./charts/values.yaml
        """
    }    

    stage('Build Image') {
	    sh "docker build -f Dockerfile.staging -t ${fullRegistryUrl}:${BUILD_NUMBER}-${namespace} ."
        sh "docker tag ${fullRegistryUrl}:${BUILD_NUMBER}-${namespace} ${fullRegistryUrl}:latest-${namespace}"
    }

    stage('Push Image') {
	    sh "docker push ${fullRegistryUrl}:${BUILD_NUMBER}-${namespace}"
        sh "docker push ${fullRegistryUrl}:latest-${namespace}"
    }   

    stage('Deploy') {
	sh """
            namespace=`echo ${namespace} | cut -d "/" -f 2`
            helm upgrade --install -n ${namespace} --create-namespace ${app} --set image.repository='${fullRegistryUrl}' --set image.tag='${BUILD_NUMBER}-${namespace}' --set ingress.enabled=true ./deployment_charts
        """
    }   
  }
}